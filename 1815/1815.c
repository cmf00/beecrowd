#include <stdio.h>
#include <string.h>

void formata(char out[], int instancia, double p) {
    char n[9];

    strcat(out, "Instancia ");
    sprintf(n, "%d", instancia);
    strcat(out, n);
    strcat(out, "\n");
    sprintf(n, "%.6lf", p);
    strcat(out, n);
    strcat(out, "\n\n");
}

void vectorMatrixMult(int n, double v[n], double A[n][n]);

int main() {
    
    int n, t, k, m, instancia = 0;
    char out[300];
    double p;
    
    for (scanf("%d", &n); n > 0;) {
        
        scanf("%d %d %d", &t, &k, &m);
        
        instancia++;
        p = 0;
        double A[n][n];
        double v[n];
        
        for (int i = 0; i < n; i++){
            v[i] = 0;
        }
        
        v[t - 1] = 1;
        
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                scanf("%lf", &A[i][j]);
            }
        }
        
        for (int i = 0; i < n; i++) {
            A[k - 1][i] = 0;
        }
        A[k - 1][k - 1] = 1;
        
        for (int i = 0; i < m; i++) {
            vectorMatrixMult(n, v, A);
        }
        
        for (int i = 0; i < k - 1; i++) {
            p += v[i];
        }
        for (int i = k; i < n; i++) {
            p += v[i];
        }
       
        scanf("%d", &n);
        formata(out, instancia, p);
    }
    printf("%s", out);
}

void vectorMatrixMult (int n, double v[n], double A[n][n]) {
    double u[n];
    double sum;
    for (int i = 0; i < n; i++) {
        u[i] = v[i];
    }
    
    for (int i = 0; i < n; i++){
        sum = 0;
            for (int j = 0; j < n; j++) {
                sum += u[j] * A[j][i];
            }
        v[i] = sum;
    }
}
