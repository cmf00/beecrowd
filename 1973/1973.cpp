#include <iostream>
#include <bits/stdc++.h>

using namespace std;

int main() {
    
    ios_base :: sync_with_stdio(0); cin.tie(0);

    long long int sobra;
    int fazendas;
    int ovelhas;
    int atacados;
    int ones;
    int i;


    while(cin >> fazendas) {
        sobra = ovelhas = atacados = ones = i = 0;
        
        // Procura o primeiro par na sequência
        for (; i < fazendas; i++) {
            atacados += 1;
            cin >> ovelhas;
            
            // Se encontrar a fazenda não tiver ovelhas,
            // rouba uma ovelhas de todas as anteriores
            // exceto das fazendas que tivessem apenas uma no início
            if (ovelhas == 0) {
                sobra += ovelhas - (i - ones);
                i++;
                break;
            }
            
            // Se o número de ovelhas for maior que zero,
            // se o número for ímpar, rouba uma ovelha,
            // senão, rouba uma ovelha desta e de todas
            // as anteriores, exceto das fazendas que tivessem
            // apenas uma no início
            if (ovelhas > 0) {
                if ((ovelhas % 2) == 0) {
                    sobra += ovelhas - (i - ones + 1);
                    i++;
                    break;
                } else
                    sobra += ovelhas - 1;
            }

            // Conta o número de fazendas que tem apenas uma
            // ovelha ao início antes de encontrar um número par.
            if (ovelhas == 1)
                ones += 1;
        }

            // Soma todas as ovelhas das fazendas subsequentes
            for (; i < fazendas; i++) {
                cin >> ovelhas;
                sobra += ovelhas;
            }
            cout << atacados << ' ' << sobra << '\n';
   }
}
